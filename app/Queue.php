<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    public function groups(){
        return $this->belongsTo('App\Group','group');
    }

    public function getUpdatedAtAttribute($value){
        $this->updated_at = strtotime($value);
    }
}
