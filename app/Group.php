<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function queue(){
        return $this->hasMany('App\Queue','group')->where('status','<','3')->orderBy('priority');
    }
    public function queue2(){
        return $this->hasMany('App\Queue','group')->where('status','>','0')->where('status','<','3')->orderBy('priority');
    }
    public function getGroupNameLabelAtrribute(){
        return $this->group;
    }
}
