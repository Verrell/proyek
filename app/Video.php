<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function getFullUrlAttribute()
    {
        return asset('video/'.$this->name);
    }
    
    public function getMediaTypeAttribute()
     {
         if($this->tipe==0){
            return "Video";
         }else{
            return "Foto";
         }
     }
     protected $appends = ['full_url','media_type'];
}
