<?php

namespace App\Http\Controllers;
use Auth;
use App\Video;
use App\Setting;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function getVideo(){
        $videos=Video::where('tipe','=',0)->get();
        return $videos;
    }

    public function getFoto(){
        $videos=Video::where('tipe','=',1)->get();
        return $videos;
    }

    public function index()
    {
        $setting=Setting::All();
        $video = Video::All();
        return view('videos.index',compact('video','setting'));
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $file = $request->file('name');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/video/';
        $file->move($path,$filename);
        $video = new Video();
        $video->name = $filename;
        $video->description = $request->description;
        $video->tipe = $request->tipe;
        $video->save();
        return redirect('/videos');
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        
        $video = Video::Find($id);
        $file = $request->file('name');
        $filename = $file->getClientOriginalName();
        $path = public_path().'/video/';
        $file->move($path,$filename);
        $video->name = $filename;
        $video->description = $request->description;
        $video->tipe = $request->tipe;
        $video->save();
        return redirect('/videos');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $video=Video::Find($id);
        $video->delete();
        return redirect('/videos');
    }
}
