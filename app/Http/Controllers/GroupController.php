<?php

namespace App\Http\Controllers;

use Auth;
use App\Group;
use App\Setting;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $setting=Setting::All();
        if (Auth::check()){
            $group=Group::All();
            return view('groups.indexadmin',compact('group','setting'));
        }else{
            return redirect('/queue');
        }
        
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $group = new Group();
        $group->group =$request->group;
        $group->min =$request->min;
        $group->max =$request->max;
        $group->save();
        return redirect('/group');
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $edit=Group::Find($id);
        $edit->group =$request->group;
        $edit->min =$request->min;
        $edit->max =$request->max;
        $edit->save();
        return redirect('/group');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
        $group=Group::Find($id);
        $group->delete();
        return redirect('/group');
    }
}
