<?php

namespace App\Http\Controllers;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function configure(){
        $setting=Setting::All();
        return view('settings.configure',compact('setting'));
    }
    public function web()
    {
        $setting=Setting::All();
        return view('settings.index',compact('setting'));
    }
    public function android()
    {
        $setting=Setting::All();
        return view('settings.android',compact('setting'));
    }
    public function index()
    {
        $setting=Setting::All();
        return view('settings.index',compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id==0){
            $setting=Setting::where('id','<','9')->get();
            $i=1;
        }
        else if($id==1){
            $setting=Setting::where('id','>=','9')->where('id','<',25)->get();
            $i=9;    
        }
        else if($id==2){
            $setting=Setting::where('id','>=','25')->get();
            $i=25;
        }
        foreach($setting as $s){
            $a="set".$i;
            $s->description=$request->$a;
            $i++;
            if ($s->description=="" && ($i==7 || $i==16)){
                continue;
            }
            if ($i==7 || $i==16){
                $file = $request->file($a);
                $filename = $file->getClientOriginalName();
                $s->description=$filename;
                $path = public_path().'/picture/';
                $file->move($path,$filename);
            }
            $s->save();
        }
        return redirect("/queue");

    }

    public function getSetting(Request $request){
        $setting=Setting::where('id','<=','14')->Get();
        return $setting;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
