<?php

namespace App\Http\Controllers;

use Twilio\Rest\Client;
require_once "C:/Users/Verrell/proyek/vendor/twilio/sdk/Twilio/autoload.php";
use Auth;
use App\Queue;
use App\Setting;
use App\Group;
use App\Report;
use App\Video;
use Illuminate\Http\Request;
use DB;
header("Access-Control-Allow-Origin: *");

class QueueController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function sendSMS($group,$prior){
        $queue=Queue::where('group','=',$group)->where('status','<',3)->where('phone','<>',"")->where('priority','=',($prior+2))->first();
        $setting=Setting::where('id','>=',25)->get();
        if (!is_null($queue)){
            $accountSid = $setting[0]->description;
            $authToken = $setting[1]->description;
            $twilioNumber = $setting[2]->description;
            $message = $setting[3]->description;
            $message = str_replace("[number]",$queue->groups->group.$queue->number,$message);
            
            $toNumber = $queue->phone;
            $client = new Client($accountSid, $authToken);
            
            try {
                $client->messages->create(
                    $toNumber,
                    [
                        "body" => $message,
                        "from" => $twilioNumber
                        //   On US phone numbers, you could send an image as well!
                        //  'mediaUrl' => $imageUrl
                        ]
                    );
                    echo 'Message sent';
                } catch (TwilioException $e) {
                    echo  $e;
                }
            }
        }

        public function coba(){
            return view('queues.coba');
        }

        public function reset(){
            $group=Group::All();
            $result = new Report();
            $hasil="{";
            $i=0;
            foreach($group as $g){
                $hasil=$hasil.'"'.$g->group.'":';
                $queue=Queue::where('group','=',$g->id)->get()->count();
                if($i==count($group)-1){
                    $hasil=$hasil.$queue."}";    
                }else{
                    $hasil=$hasil.$queue.",";
                }
                $i++;
            }
            $result->quantity=$hasil;
            $result->save();
            //Queue::truncate();
            return redirect('/queue');
        }

        public function getAjax(Request $request){
            $queue=array();
            $group=Group::All();
            foreach($group as $g){
                $queue1=DB::table('queues')
                ->join('groups','queues.group','=','groups.id')
                ->select('queues.number as number', 'groups.group as group','groups.spark as updated_at')
                ->where('queues.group','=',$g->id)
                ->where('queues.status','>','0')
                ->where('queues.status','<','3') 
                ->first();
                $queue[]=$queue1;
            }
            return $queue;
        }
        
        public function getAjaxAdmin(Request $request){
            $queue=array();
            $group=Group::All();
            foreach($group as $g){
                $queue1=DB::table('queues')
                ->join('groups','queues.group','=','groups.id')
                ->select('queues.number as number', 'groups.group as group','queues.status as status','queues.id as idq','groups.id as idg','groups.min as min','groups.max as max')
                ->where('queues.group','=',$g->id)
                ->where('queues.status','<','3')
                ->orderBy('priority')
                ->get();
                $queue[]=$queue1;
            }
            return $queue;
        }

        public function indexCustomer($id="0"){
            $setting=Setting::All();
            $queue=array();
            $count=array();
            $group=Group::All();
            $chat=0;
            foreach($group as $g){
                $queue1=$g->queue->where('status','>',0)->where('status','<',3);
                $count[]=count($queue1);
                $queue[]=$queue1;
            }
            if ($id=="0"){
                $id="0";
                return view('queues.index',compact('group','queue','id','count','setting','chat'));
            }else{
                $q = Queue::where('token','=',$id)->first();
                if(!is_null($q)){
                    if($q->phone != ""){
                        $id="0";
                    }
                    $chat=1;
                }else{
                    $id="0";
                    return redirect('/public');
                }
                return view('queues.index',compact('group','queue','id','count','setting','chat'));

            }
        }
        
        public function getToken(Request $request){
            $group = $request->id;
            $number = substr($group,1,strlen($group)-1);
            $group = $group[0];
            $huruf = Group::where('group','=',$group)->first();
            $queue = Queue::where('group','=',$huruf->id)->where('number','=',$number)->first();
            return ($queue->token);
        }
        
        public function getQueue(Request $request){
            $queue = new Queue();
            $queue->person =$request->person;
            $groupid = Group::where('min','<=',$request->person)->where('max','>=',$request->person)->first();
            if (is_null($groupid)){
                $groupid = Group::select('id')->orderBy('id','desc')->first();
            }
            $queue->group = $groupid->id;
            $queue->status = 0;
            $queuepr = Queue::where('status','<',3)->where('group','=',$groupid->id)->orderBy('priority','desc')->first();
            if(is_null($queuepr)){
                $queue->priority = 1;
            }else{
                $queue->priority = $queuepr->priority+1;
            }
            $queuen = Queue::where('group','=',$groupid->id)->orderBy('number','desc')->first();
            if(is_null($queuen)){
                $queue->number = 1;
            }else{
                $queue->number = $queuen->number+1;
            }
            $queue->token = md5($queue->groups->group.$queue->number);
            $queue->save();
            return ($queue->groups->group.$queue->number);
        }
        
        public function index(Request $request)
        {
            $setting=Setting::All();
            $queue=array();
            $group=Group::All();
            foreach($group as $g){
                $queue1=$g->queue;
                $queue[]=$queue1;
            }
            return view('queues.indexadmin',compact('group','queue','setting'));
        }
        
        /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
        public function create()
        {
            //
        }
        
        /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
        public function store(Request $request)
        {
            //
        }
        
        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show($id)
        {
            //
        }
        
        /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function edit($id)
        {
            
        }
        
        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function updatephone(Request $request, $id)
        {
            $edit=Queue::where('token','=',$id)->first();
            $nomor="+62".$request->phone;
            $edit->phone = $nomor;
            $edit->save();
            return redirect('/public/'.$id);
        }

        public function updatespark(Request $request)
        {
            $group=Group::Find($request->id);
            $group->spark=0;
            $group->save();
            return ("haha");
        }

        public function update(Request $request, $id)
        {
            $edit=Queue::Find($id);
            $type=$request->type;
            if($edit->status==2){
                $type=6;
            }
            if($type<4){
                $queue=Group::Find($edit->group);
                $queue=$queue->queue->where('status','<',2);
                foreach($queue as $q){
                    $q->status=0;
                    $q->save();
                }
            }
            if($type==0){
                $edit1=Queue::where('status','<',2)->where('group','=',$edit->group)->where('priority','<',$edit->priority)->orderBy('priority','desc')->Get();
                if(count($edit1)!=0){
                    $editp=$edit->priority;
                    $edit->priority=$edit1[0]->priority;
                    $edit1[0]->priority=$editp;
                    $edit1[0]->save();
                }
            }else if($type==1){
                $group=Group::Find($edit->group);
                $edit1=$group->queue->where('status','<',2)->First();
                if($edit->id!=$edit1->id){
                    $edit->priority=$edit1->priority;
                    $editsisa=Queue::where('status','<',2)->where('group','=',$edit->group)->orderBy('priority')->get();
                    $p=$edit1->priority+1;
                    foreach($editsisa as $q){
                        if($edit->id!=$q->id){
                            $q->priority=$p;
                            $p=$p+1;
                            $q->save();
                        }
                    }
                }
            }else if($type==2){
                $edit1=Queue::where('status','<',2)->where('group','=',$edit->group)->where('priority','>',$edit->priority)->orderBy('priority')->Get();
                if(count($edit1)!=0){
                    $editp=$edit->priority;
                    $edit->priority=$edit1[0]->priority;
                    $edit1[0]->priority=$editp;
                    $edit1[0]->save();
                }
            }else if($type==3){
                $edit1=Queue::where('status','<',2)->where('group','=',$edit->group)->where('priority','>=',$edit->priority)->orderBy('priority','desc')->First();
                if($edit->id!=$edit1->id){
                    $edit->priority=$edit1->priority;
                    $p=$edit->priority-1;
                    $editsisa=Queue::where('status','<',2)->where('group','=',$edit->group)->orderBy('priority','desc')->get();
                    foreach($editsisa as $q){
                        if($edit->id!=$q->id){
                            $q->priority=$p;
                            $p=$p-1;
                            $q->save();
                        }
                    }
                }
            }else if($type==4){
                if ($edit->status==0){
                    $edit2=Queue::where('status','=',2)->where('group','=',$edit->group)->orderBy('priority')->Get();
                    if(count($edit2)!=0){
                        if($edit->id!=$edit2[0]->id){
                            $edit2[0]->status=3;   
                            $edit2[0]->save();
                        }
                    }
                    $edit->status=1;
                    $edit1=Queue::where('status','=',1)->where('group','=',$edit->group)->orderBy('priority')->Get();
                    if(count($edit1)!=0){
                        if($edit->id!=$edit1[0]->id){
                            $edit1[0]->status=0;
                            $edit1[0]->save();
                        }
                    }
                    $group=Group::Find($edit->group);
                    $group->spark=1;
                    $group->save();
                    $edit1=$group->queue->where('status','<',2)->First();
                    if($edit->id!=$edit1->id){
                        $edit->priority=$edit1->priority;
                        $editsisa=Queue::where('status','<',2)->where('group','=',$edit->group)->orderBy('priority')->get();
                        $p=$edit1->priority+1;
                        foreach($editsisa as $q){
                            if($edit->id!=$q->id){
                                $q->priority=$p;
                                $p=$p+1;
                                $q->save();
                            }
                        }
                    }
                }
            }else if($type==5){
                if ($edit->status==1){
                    $edit->status=2;
                }
            }
            $edit->save();
            if($type==4){
                $aa=$this->sendSMS($edit->group,$edit->priority);
            }
            return redirect('/queue');
        }
        
        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy($id)
        {
            //
        }
    }
    