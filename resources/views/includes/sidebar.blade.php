  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>

        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
    
        <li id="queuePage">
          <a href="/queue/">
            <i class="fa fa-files-o"></i>
            <span>Queue</span>
          </a>
        </li>
        @if(Auth::user()->role==1 )
        <li id="groupPage">
          <a href="/group/">
            <i class="fa fa-files-o"></i>
            <span>Group</span>
          </a>
        </li>
        <li id="videoPage">
          <a href="/videos">
            <i class="fa fa-files-o"></i>
            <span>Media</span>
          </a>
        </li>
        <li id="reportPage">
          <a href="/report">
            <i class="fa fa-files-o"></i>
            <span>Report</span>
          </a>
        </li>
        <li id="userPage">
          <a href="/admin">
            <i class="fa fa-files-o"></i>
            <span>User</span>
          </a>
        </li>
        <li  id="settingPage" class="treeview ">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
          <li><a href="/setting"><i class="fa fa-circle-o"></i>Web Interface</a></li>
          <li><a href="/android"><i class="fa fa-circle-o"></i>Android Interface</a></li>
            <li><a href="/configure"><i class="fa fa-circle-o"></i>Configuration</a></li>
          </ul>
        </li>
        @endif
        <li>
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>