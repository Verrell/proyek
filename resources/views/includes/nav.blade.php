@include("includes.head")
<nav class="navbar-inverse" style="margin-bottom: 20px">
  <div class="container">
    <div class="navbar-header" >
      <a class="navbar-brand" href="/queue">{{$setting[11]->description}}</a>
    </div>
    <ul class="nav navbar-nav navbar-right" style="font-size:20px;">
      
      <li  id="queuePage"><a href="/queue">Queue</a></li>
      @if(Auth::user()->role==1 )
      <li id="groupPage"><a href="/group">Group</a></li>
      <li  id="videoPage"><a href="/videos">Video</a></li>
      <li id="userPage"><a href="/admin">User</a></li>
      <li id="reportPage"><a href="/report">Report</a></li>
      <li class="dropdown nav navbar-nav">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
              Setting<span class="caret"></span>
            </a>
        <ul class="dropdown-menu">
          <li  id="settingPage"><a href="/setting">Interface</a></li> 
          <li  id="settingPage"><a href="/configure">Configuration</a></li> 
      </ul>
       </li>
      @endif
      <li class="dropdown nav navbar-nav navbar-right">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
          {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
          <li>
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </li>
  </ul>

</div>
</nav>
@include('includes.style',compact('setting'))