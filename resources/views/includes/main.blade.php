<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
	@yield('head')

	<!--@include('includes.style')-->
</head>

<body class="hold-transition skin-blue sidebar-mini fixed">

<div class="wrapper">
		@include('includes.sidebar')
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				@yield('path')
			</section>
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-sm-12">
						@yield('content')
					</div>
				</div>
			</section>
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.4.0
			</div>
			<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
			reserved.
		</footer>

		<!-- Control Sidebar -->
		
</div>
@yield('foot')
@include('includes.foot')
</body>
</html>
