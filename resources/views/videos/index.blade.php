@extends('includes.main')
@section('head')

<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('path')
<h1>
  Media
  <small>Manage Media</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="#">Media</a></li>
  <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Media Form</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action='' method='POST' enctype="multipart/form-data">
      <div class="box-body">
        <div class="form group col-sm-12">
          <label> File: </label>  
          <input type="file" class="form-control" name="name" required>
        </div>
        <div class="form group col-sm-12">
          <label>Description</label>
          <input class="form-control" type='text' name='description' required>
        </div>
        <div class="form group col-sm-12">
          <label>Tipe Media</label><br>
          <div class="form group col-sm-3">
            <input class="radio-inline" type='radio' name='tipe' value="0" required>Video
          </div>

          <div class="form group col-sm-3">
            <input class="radio-inline" type='radio' name='tipe' value="1" required>Foto
          </div>
        </div>
      </div>
      <div class="box-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form group col-sm-12">
          <button type="submit" class="btn pull-right myButton1"> Add</button>
        </div>
      </div>
    </form>
  </div>

  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Media Table</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          @foreach($video as $v)
          <tr>
            <td>{{$v->id}}</td>
            <td>{{$v->name}}</td>
            <td>{{$v->description}}</td>
            <td>{{$v->media_type}}</td>
            <td><button class="btn btn-warning" data-hover="tooltip" data-placement="top" data-target="#editModal" data-toggle="modal" id="modal-edit" data-id={{$v->id}} data-name={{$v->name}} data-desc={{$v->description}}  title="Edit" >Edit</button>
            </td>
            <td><button class="btn btn-danger" data-hover="tooltip" data-placement="top" data-target="#deleteModal" data-toggle="modal" id="modal-edit" data-id={{$v->id}} data-name={{$v->name}} data-desc={{$v->description}} title="Delete" >Delete</button></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--EDIT MODAL-->
<div class="modal modal-warning fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Group</h4>
        </div>

        <form action='' method='POST' id="search" enctype="multipart/form-data">
          <div class="modal-body col-sm-12">
            <div class="form group col-sm-12">
              <label>Name</label>
              <input type="file" class="form-control"  name="name" required>
            </div>
            <div class="form group col-sm-12">
              <label>Description</label>
              <input class="form-control" id="desc" type='text' name='description' required>
            </div>
            <div class="form group col-sm-12">
              <label>Tipe Media</label><br>
              <input class="radio-inline" id="tipe" type='radio' value="0" name='tipe' required>Video
              <input class="radio-inline" id="tipe" type='radio' value="1" name='tipe' required>Foto
            </div>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </div>
          <div class="modal-footer col-sm-12">
            <button type="submit" class="btn btn-warning pull-right"> Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--END EDIT MODAL-->
  <!-- DELETE MODAL -->
  <div class="modal modal-danger fade" id="deleteModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Group</h4>
          </div>

          <form action='' method='POST' id="search2" enctype="multipart/form-data">
            <div class="modal-body col-sm-12">
              <div class="form group col-sm-12">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="name" readonly>
              </div>
              <div class="form group col-sm-12">
                <label>Description</label>
                <input class="form-control" id="desc" type='text' name='description' readonly>
              </div>
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="modal-footer col-sm-12">
              <button type="submit" class="btn btn-danger pull-right"> Delete</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END DELETE MODAL -->
  </div>
  @stop
  @section('foot')
  <script type="text/javascript">
    $(function() {
      document.getElementById("videoPage").className += " active";
    });
    $('#editModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('id') // Extract info from data-* attributes
      var name = button.data('name')
      var description = button.data('desc')
      var modal = $(this)
      frm = document.getElementById('search');
      frm.action="/videos/"+id;
      modal.find('#name').val(name);
      modal.find('#desc').val(description);
    })
    $('#deleteModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('id') // Extract info from data-* attributes
      var name = button.data('name')
      var description = button.data('desc')
      var modal = $(this)
      frm = document.getElementById('search2');
      frm.action="/videos/"+id;
      modal.find('#name').val(name);
      modal.find('#desc').val(description);
      
    })
  </script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
    })
  })
</script>-->
@stop