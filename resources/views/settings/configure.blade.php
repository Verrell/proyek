@extends('includes.main')
@section('head')

<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('path')
<h1>
  Configuration
  <small>Manage Configuration</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="#">Configuration</a></li>
  <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Queue Form</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action='/reset' method='POST'>
      <div class="box-body">
        <div class="form group col-sm-12" style="margin-top: 20px">
          <label>Reset Queue Number</label>
          
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-danger pull-right" > Reset</button>
        </div>
      </div>
    </form>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Twilio Form</h3>
    </div>
    <form action="{{route('setting.update',2)}}" method='POST'>
      <div class="box-body">
        <div class="form group col-sm-12">
          <label>Twilio SID</label>
          <input class="form-control" type='text' name="set25" required value={{$setting[24]->description}}>
        </div>
        
        <div class="form group col-sm-12">
          <label>Twilio Token</label>
          <input class="form-control" type='text' name='set26' required value={{$setting[25]->description}}>
        </div>
        
        <div class="form group col-sm-12">
          <label>Twilio Number</label>
          <input class="form-control" type='text' name='set27' required value={{$setting[26]->description}}>
        </div>
        <div class="form group col-sm-12">
          <label>Message</label>
          <textarea class="form-control" id="text" name="set28" placeholder="Type in your message" rows="2" style="resize: none">{{$setting[27]->description}}</textarea>
          <h6 class="pull-right" id="count_message"></h6>
          <h6 class="pull-right">[number] untuk nomer antrian |</h6>
        </div>
      </div>
      <div class="box-footer">
        <div class="form group col-sm-12">
          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn pull-right myButton1" > Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
@stop
@section('foot')
<script type="text/javascript">
  $(function() {
    document.getElementById("settingPage").className += " active";
  });
  
  var text_max = 160;
  $('#count_message').html(text_max + ' remaining');
  
  $('#text').keyup(function() {
    var text_length = $('#text').val().length;
    var text_remaining = text_max - text_length;
    
    $('#count_message').html(text_remaining + ' remaining');
  });
</script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>-->
  @stop