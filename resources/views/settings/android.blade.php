@extends('includes.main')
@section('head')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@stop
@section('path')
<h1>
  Interface
  <small>Manage Interface</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="#">Interface</a></li>
  <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Android Interface</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start --> 
    <form action="{{route('setting.update',0)}}" method="POST"  enctype="multipart/form-data">
      <div class="box-body">
        <div class=col-sm-6>
          <label>Warna Background</label>
          <div id="cp1" class="input-group colorpicker-component">
            <input type="text" class="form-control input-lg" value={{$setting[0]->description}} name="set1" />
            <span class="input-group-addon"><i></i></span>
          </div>
          <label>Warna Button Gradasi 1</label>
          <div id="cp1" class="input-group colorpicker-component">
            <input type="text" class="form-control input-lg" value={{$setting[1]->description}} name="set2" />
            <span class="input-group-addon"><i></i></span>
          </div>
          <label>Warna Button Gradasi 2</label>
          <div id="cp1" class="input-group colorpicker-component">
            <input type="text" class="form-control input-lg" value={{$setting[2]->description}} name="set3">
            <span class="input-group-addon"><i></i></span>
          </div>
        </div>
        <div class="col-sm-6">
          <label>Warna Background Input</label>
          <div id="cp1" class="input-group colorpicker-component">
            <input type="text" class="form-control input-lg" value={{$setting[3]->description}} name="set4">
            <span class="input-group-addon"><i></i></span>
          </div>
          <label>Warna Font</label>
          <div id="cp1" class="input-group colorpicker-component">
            <input type="text" class="form-control input-lg" value={{$setting[4]->description}} name="set5">
            <span class="input-group-addon"><i></i></span>
          </div>
        </div>
        <div class="col-sm-12">
          <label>Logo Restoran</label>
          <input type="file" class="form-control" value={{$setting[5]->description}} name="set6">
          <label>Current Logo</label><br>
          @if($setting[5]->description=="")
          Nothing to display.<br>
          @else
          <img src="{{URL::asset('picture/'.$setting[5]->description)}}" style="width:10vw;height:10vh;" alt="Can not load">
          <br>
          @endif
          <label>Keterangan</label>
          <textarea class="form-control" name="set7" style="resize:none;" required>{{$setting[6]->description}}</textarea>
          <label>Struk</label>
          <textarea class="form-control" name="set8" style="resize:none;" required>{{$setting[7]->description}}</textarea>
        </div>
      </div>
      <div class="box-footer">
        <div class="form group col-sm-12">
          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn pull-right myButton1" > Save changes</button>
        </div>
      </div>
    </form>
  </div>
</div>
@stop
@section('foot')

<script>
  $(function () {
    $('#cp1,#cp2').colorpicker({
      autoInputFallback: false
    });
  });
</script>
<script type="text/javascript">
  $(function() {
    document.getElementById("settingPage").className += " active";
  });
</script>

<script>
  $(function () {
    $('#cp13').colorpicker({
      autoInputFallback: false
    });
  });
</script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
    })
  })
</script>-->
@stop