@extends('includes.main')
@section('path')
<h1>
    Interface
    <small>Manage Interface</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Interface</a></li>
    <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Web Interface</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start --> 
        <form action="{{route('setting.update',1)}}" method="POST"  enctype="multipart/form-data">
            <div class="box-body">
                <div class="col-sm-12">
                    <label>Nama Restoran</label>
                    <textarea class="form-control" name="set14" style="resize:none;" required>{{$setting[13]->description}}</textarea>
                    
                    <label>Banner Restoran</label>
                    <input type="file" class="form-control" value={{$setting[14]->description}} name="set15">
                    <label>Current Banner</label><br>
                    @if($setting[14]->description=="")
                    Nothing to display.<br>
                    @else
                    <img src="{{URL::asset('picture/'.$setting[14]->description)}}" style="width:10vw;height:10vh;" alt="Can not load">
                    @endif
                </div>
                
                <div class=col-sm-6>
                    <label>Navbar</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[8]->description}} name="set9">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Background</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[9]->description}} name="set10">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Warna Blinking Queue</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[23]->description}} name="set24">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Background Group 1</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[15]->description}} name="set16">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Background Group 2</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[16]->description}} name="set17">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Background Group 3</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[17]->description}} name="set18">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Background Group 4</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[18]->description}} name="set19">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <div class=col-sm-6>
                    <label>Button</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[10]->description}} name="set11">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Font</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[11]->description}} name="set12">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Panel</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[12]->description}} name="set13">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Font Group 1</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[19]->description}} name="set20">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Font Group 2</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[20]->description}} name="set21">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Font Group 3</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[21]->description}} name="set22">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                    <label>Font Group 4</label>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" class="form-control input-lg" value={{$setting[22]->description}} name="set23">
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form group col-sm-12">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn pull-right myButton1" > Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('foot')
<script>
    $(function () {
        $('#cp1,#cp2').colorpicker({
            autoInputFallback: false
        });
    });
</script>

<script type="text/javascript">
    function yaya(){
        alert("ha");
    }
    $(function() {
        document.getElementById("settingPage").className += " active";
    });

</script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>-->
    @stop