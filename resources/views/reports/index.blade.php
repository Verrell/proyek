@extends('includes.main')
@section('head')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="../../bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<style>
        .legend1{
            height: 100%;
            background-color: #f56954;
        }
        
        .legend2{
            height: 100%;
            background-color: #00a65a;
        }
        
        .legend3{
            height: 100%;
            background-color: #f39c12;
        }
        
        .legend4{
            height: 100%;
            background-color: #00c0ef;
        }
</style>

@stop
@section('path')
<h1>
    Report
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
    <!-- general form elements -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Report Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            @foreach($group as $g)
            <div class="col-sm-3" style="height:4vmin;">
                <div class="col-sm-5" style="text-align:right;">
                    {{$g->group}}
                </div>
                <div class="col-sm-4 legend{{$g->id}}" id="legend{{$g->id}}">
                    
                </div>
            </div>
            @endforeach
            
            @for($i=0; $i<count($report); $i++)
            <div class="chart">
                {{$report[$i]->created_at}}
                <canvas id="pieChart{{$i}}" style="height: 129px; width: 287px;" width="861" height="387"></canvas>
            </div>
            @endfor
        </div>
    </div>
</div>
<!--EDIT MODAL-->
<div class="modal modal-warning fade" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Group</h4>
                </div>
                
                <form action='' method='POST' id="search" enctype="multipart/form-data">
                    <div class="modal-body col-sm-12">
                        <div class="form group col-sm-12">
                            <label>Group</label>
                            <input class="form-control group" id="group" type='text' name='group' required>
                        </div>
                        <div class="form group col-sm-6">
                            <label>Min</label>
                            <input class="form-control" id="min" type='text' name='min' required>
                        </div>
                        <div class="form group col-sm-6">
                            <label>Max</label>
                            <input class="form-control" id="max" type='text' name='max' required>
                        </div>
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="modal-footer col-sm-12">
                        <button type="submit" class="btn btn-warning pull-right"> Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--END EDIT MODAL-->
    <!-- DELETE MODAL -->
    <div class="modal modal-danger fade" id="deleteModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete Group</h4>
                    </div>
                    
                    <form action='' method='POST' id="search2" enctype="multipart/form-data">
                        <div class="modal-body col-sm-12">
                            <div class="form group col-sm-12">
                                <label>Group</label>
                                <input class="form-control group" id="group" type='text' name='group' readonly>
                            </div>
                            <div class="form group col-sm-6">
                                <label>Min</label>
                                <input class="form-control" id="min" type='text' name='min' readonly>
                            </div>
                            <div class="form group col-sm-6">
                                <label>Max</label>
                                <input class="form-control" id="max" type='text' name='max' readonly>
                            </div>
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="modal-footer col-sm-12">
                            <button type="submit" class="btn btn-danger pull-right"> Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END DELETE MODAL -->
    </div>
    @stop
    @section('foot')
    <script type="text/javascript">
        $(function() {
            document.getElementById("reportPage").className += " active";
            ajaxReport();
            function ajaxReport() {
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: '{{route("API.report")}}',
                    success: function(data) {
                        for (var i =0; i< data.length; i++){
                            var pieChartCanvas = $('#pieChart'+i).get(0).getContext('2d');
                            var pieChart       = new Chart(pieChartCanvas);    
                            var PieData        = [];
                            
                            var obj = data[i].quantity;
                            obj = JSON.parse(obj);
                            var keys = Object.keys(obj);
                            var val = Object.values(obj);
                            
                            for (var j=0; j<keys.length; j++){
                                var doc = document.getElementById("legend"+(j+1));
                                var cs=document.defaultView.getComputedStyle(doc,null);
                                var bg=cs.getPropertyValue('background-color');
                                var rep={
                                    value    : val[j],
                                    color    : bg,
                                    highlight: bg,
                                    label    : keys[j]
                                }
                                PieData.push(rep);
                            }

                            var pieOptions     = {
                                //Boolean - Whether we should show a stroke on each segment
                                segmentShowStroke    : true,
                                //String - The colour of each segment stroke
                                segmentStrokeColor   : '#fff',
                                //Number - The width of each segment stroke
                                segmentStrokeWidth   : 2,
                                //Number - The percentage of the chart that we cut out of the middle
                                percentageInnerCutout: 50, // This is 0 for Pie charts
                                //Number - Amount of animation steps
                                animationSteps       : 100,
                                //String - Animation easing effect
                                animationEasing      : 'easeOutBounce',
                                //Boolean - Whether we animate the rotation of the Doughnut
                                animateRotate        : true,
                                //Boolean - Whether we animate scaling the Doughnut from the centre
                                animateScale         : false,
                                //Boolean - whether to make the chart responsive to window resizing
                                responsive           : true,
                                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                                maintainAspectRatio  : true,
                                //String - A legend template
                                legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
                            }
                            //Create pie or douhnut chart
                            // You can switch between pie and douhnut using the method below.
                            pieChart.Doughnut(PieData, pieOptions)
                        }    
                    }
                });
            }
            
        });
    </script>
    <!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script>
            $(function () {
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : false,
                    'ordering'    : true,
                    'info'        : true,
                    'autoWidth'   : false
                })
            })
        </script>-->
        @stop