<!DOCTYPE html>
<head>
    @include("includes.head2")
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include("includes.style")
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            @if(isset($id))
            <input type="hidden" name="_token" id="code" value={{$id}}>
            @else
            <input type="hidden" name="_token" id="code" value="0">
            @endif
            
            <div class="col-sm-6" style="height:99.5vh;padding:0px!important;" hidden id="video">
                <div class="col-sm-12" style="height:49.5vh;padding:0px!important;" id="banner">
                    <img src="{{URL::asset('picture/'.$setting[14]->description)}}" style="width:100%;height:100%;" alt="Can not load">
                </div>
                <div class="col-sm-12" style="height:50vh;padding:0px!important;">
                    <video id="myVideo" style="width:100%;height:100%;" autoplay>
                        <source src="" id="mp4_src" type="video/mp4">
                        </video>
                    </div>
                </div>    
                <div class="col-sm-6" style="height:50vh;padding:0px!important" hidden id="picture">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        
                    </div>
                </div>
                <div class="col-sm-6" style="padding:0px!important">
                    <div class="col-xs-12" style="padding:0px;margin:0px!important">
                        @php($i=0)
                        @foreach ($group as $g)
                        <div class="col-xs-12 myQueue{{$g->id}}" id="myQueue{{$g->id}}">
                            <div class="col-xs-8">
                                @if($i==count($group)-1)
                                {{$g->group}} ({{$g->min}}+)
                                @else
                                {{$g->group}} ({{$g->min}} - {{$g->max}})
                                @endif
                                @php($i++)
                            </div>
                            @if(count($g->queue2)==0)
                            <div class="col-xs-4" id="myFont{{$g->id}}">
                                -
                            </div>
                            @else
                            <div class="col-xs-4" id="myFont{{$g->id}}">
                                {{$g->group}}{{$g->queue2[0]->number}}
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Notification</h4>
                    </div>
                    
                    <div class="modal-body col-sm-12">
                        
                        <form action="" method='POST' id="search">
                            <div class="form group col-sm-12">
                                Do you want to get notified via SMS ?
                            </div>
                            <div class="form group col-sm-12">
                                <label>Please enter your phone number</label>
                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div class="input-group-addon">+62</div>
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                                
                            </div>
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                            <button type="submit" class="btn btn-warning pull-right">Yes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
    <script>
        !function(a){var b=/iPhone/i,c=/iPod/i,d=/iPad/i,e=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,f=/Android/i,g=/(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,h=/(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,i=/IEMobile/i,j=/(?=.*\bWindows\b)(?=.*\bARM\b)/i,k=/BlackBerry/i,l=/BB10/i,m=/Opera Mini/i,n=/(CriOS|Chrome)(?=.*\bMobile\b)/i,o=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,p=new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),q=function(a,b){return a.test(b)},r=function(a){var r=a||navigator.userAgent,s=r.split("[FBAN");return"undefined"!=typeof s[1]&&(r=s[0]),s=r.split("Twitter"),"undefined"!=typeof s[1]&&(r=s[0]),this.apple={phone:q(b,r),ipod:q(c,r),tablet:!q(b,r)&&q(d,r),device:q(b,r)||q(c,r)||q(d,r)},this.amazon={phone:q(g,r),tablet:!q(g,r)&&q(h,r),device:q(g,r)||q(h,r)},this.android={phone:q(g,r)||q(e,r),tablet:!q(g,r)&&!q(e,r)&&(q(h,r)||q(f,r)),device:q(g,r)||q(h,r)||q(e,r)||q(f,r)},this.windows={phone:q(i,r),tablet:q(j,r),device:q(i,r)||q(j,r)},this.other={blackberry:q(k,r),blackberry10:q(l,r),opera:q(m,r),firefox:q(o,r),chrome:q(n,r),device:q(k,r)||q(l,r)||q(m,r)||q(o,r)||q(n,r)},this.seven_inch=q(p,r),this.any=this.apple.device||this.android.device||this.windows.device||this.other.device||this.seven_inch,this.phone=this.apple.phone||this.android.phone||this.windows.phone,this.tablet=this.apple.tablet||this.android.tablet||this.windows.tablet,"undefined"==typeof window?this:void 0},s=function(){var a=new r;return a.Class=r,a};"undefined"!=typeof module&&module.exports&&"undefined"==typeof window?module.exports=r:"undefined"!=typeof module&&module.exports&&"undefined"!=typeof window?module.exports=s():"function"==typeof define&&define.amd?define("isMobile",[],a.isMobile=s()):a.isMobile=s()}(this);
        
        $(document).ready(function(){
            var id = document.getElementById("code").value;
            if(isMobile.any){
                document.getElementById("picture").hidden=false;
                ajaxFoto();
            }else{
                document.getElementById("video").hidden=false;
                ajaxVideo();
            }
            if ( id != "0" ){
                $('#exampleModal').modal('show');
            }
            
            if ( {{$chat}}==1 ){
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                    s1.async=true;
                    s1.src='https://embed.tawk.to/5a5881c44b401e45400c080b/default';
                    s1.charset='UTF-8';
                    s1.setAttribute('crossorigin','*');
                    s0.parentNode.insertBefore(s1,s0);
                })();
            }
            
            var videoSource = new Array();
            var j= 0; 
            var videoCount;
            function ajaxVideo() {
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: '{{route("API.video")}}', 
                    success: function(data) {
                        for (var i=0; i<data.length;i++){
                            videoSource[i]= data[i].full_url;
                        }
                        videoCount = videoSource.length;
                        videoPlay(0);
                    }
                });
            }
            
            function ajaxFoto() {
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: '{{route("API.foto")}}', 
                    success: function(data) {
                        html='<ol class="carousel-indicators">';
                            html+='</ol>'
                            html+='<div class="carousel-inner">';
                                for(i=0;i<data.length;i++){
                                    if(i==0){
                                        html+='<div class="item active"><img src="'+data[i].full_url+'" style="width:100vmin;height:50vh;" alt="Can not load"></div>';
                                    }else{
                                        html+='<div class="item"><img src="'+data[i].full_url+'" style="width:100vmin;height:50vh;" alt="Can not load"></div>';
                                    }
                                }
                                html+='</div>';
                                document.getElementById("myCarousel").innerHTML = html;
                            }
                        });
                    }
                    
                    function videoPlay(videoNum) {
                        document.getElementById("mp4_src").src = videoSource[videoNum];
                        document.getElementById("myVideo").load();
                        document.getElementById("myVideo").play();
                    }
                    document.getElementById('myVideo').addEventListener('ended', myHandler, false);
                    
                    function myHandler() {
                        j++;
                        if (j == (videoCount)) {
                            j = 0;
                            videoPlay(j);
                        } else {
                            videoPlay(j);
                        }
                    }
                    
                    var change=["","","",""];

                    function ajaxinit(){
                        $.ajax({
                            type: 'GET',
                            datatype: 'json',
                            url: '{{route("API.ajax")}}',
                            success: function(data) {
                                for(var i=0; i<data.length; i++){
                                    if(data[i]==null){
                                        change[i]="";
                                    }else{
                                        change[i]=data[i].number;
                                    }
                                }
                            }
                        });
                    }

                    ajaxinit();
                    setInterval(ajaxQueue, 1000);

                    function ajaxQueue() {
                        $.ajax({
                            type: 'GET',
                            datatype: 'json',
                            url: '{{route("API.ajax")}}',
                            success: function(data) {
                                html="";
                                for (var i =0; i< data.length;i++){
                                    if(data[i]==null){
                                        html+='<div class="col-xs-12 myQueue'+(i+1)+'">-</div>';
                                        change[i]="";
                                    }else{
                                        if(change[i]!=data[i].number){
                                            document.getElementById('myFont'+(i+1)).innerHTML=data[i].group+data[i].number; 
                                            change[i]=data[i].number;
                                            sparkling2(i);
                                        }
                                        //if(data[i].updated_at==1){        
                                        //    updatespark(i+1);
                                        //    sparkling2(i);
                                        //}
                                    }
                                }
                            }
                        });
                    }

                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    function updatespark(i) {
                        $.ajax({
                            type: 'POST',
                            datatype: 'json',
                            url: '{{route("API.spark")}}',
                            data: {id:i,_method:'PUT',_token:CSRF_TOKEN},
                            success: function(data) {

                            }
                        });
                    }

                    var inte =["","","",""];
                    function sparkling2(i){
                        if(inte[i]==""){
                            inte[i] =setInterval(function(){sparkling(i+1)},1000);
                        }
                    }
                    var spar = [0,0,0,0];
                    function sparkling(i){
                        if(spar[i-1]%2==0){
                            document.getElementById('myQueue'+i).classList.add('blink');
                        }
                        else{
                            document.getElementById('myQueue'+i).classList.remove('blink');
                        }
                        spar[i-1]++;
                        if(spar[i-1]>3){
                            spar[i-1]=0;clearInterval(inte[i-1]);
                            inte[i-1]="";}
                        }
                    })
                </script>
                </html>