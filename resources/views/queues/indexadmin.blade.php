@extends('includes.main')
@section('head')

<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('path')
<h1>
    Queue
    <small>Manage Queue</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Queue</a></li>
    <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-12">
    <!-- general form elements -->
    <div id="alert" style="top:0;position:absolute;z-index:100;" class="col-sm-6 col-sm-offset-3"></div>
    
    <div class="box" id='table'>
        @php($i=0)
        @foreach($group as $g)
        <div class='col-sm-3'>
            <div class="col-sm-12 myCol1  bg-primary">
                <form action='' onsubmit='return isvalid()' method='POST' id=edit{{$g->id}} >
                    <input type='hidden' id=group{{$g->id}} readonly required>
                    <input type='hidden' id=type{{$g->id}} name='type' readonly required>
                    @if($i==count($group)-1)
                    <center><h1>{{$g->group}} ({{$g->min}}+) {{count($queue[$i++])}}</h1></center>
                    @else
                    <center><h1>{{$g->group}} ({{$g->min}}-{{$g->max}}) {{count($queue[$i++])}}</h1></center>
                    @endif
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-sm-12">
                        <button class='btn btn-primary' type='submit' onclick='return edittype(0,{{$g->id}})'><span class='glyphicon glyphicon-chevron-up'></span></button>
                        <button class='btn btn-primary' type='submit' onclick='return edittype(1,{{$g->id}})'><span class='glyphicon glyphicon-arrow-up'></span></button>
                        <button class='btn btn-primary' type='submit' onclick='return edittype(2,{{$g->id}})'><span class='glyphicon glyphicon-chevron-down'></span></button>
                        <button class='btn btn-primary' type='submit' onclick='return edittype(3,{{$g->id}})'><span class='glyphicon glyphicon-arrow-down'></span></button>
                    </div>
                    <div class="col-sm-12">
                        <button class='btn btn-success col-sm-4 col-sm-offset-1' type='submit' onclick='return edittype(4,{{$g->id}})'><span class='glyphicon glyphicon-earphone'></span></button>
                        <button class='btn btn-danger col-sm-4 col-sm-offset-2' type='submit' onclick='return edittype(5,{{$g->id}})'><span class='glyphicon glyphicon-ok'></span></button>
                    </div>
                </form>
                
                <div class="col-sm-12 myCol2" style="overflow-y:scroll;height: 50vh">
                    @foreach($g->queue as $q)
                    <div class="col-sm-12" style="height:10px;"></div>
                    @if ($q->status==0)
                    <button class="col-sm-12 btn btn-danger myButton" onclick='return select({{$q->groups->id}},{{$q->id}})' >
                        @elseif($q->status==1)
                        <button class="col-sm-12 btn btn-warning myButton" onclick='return select({{$q->groups->id}},{{$q->id}})' >
                            @elseif($q->status==2)
                            <button class="col-sm-12 btn-success btn myButton"  onclick='return select({{$q->groups->id}},{{$q->id}})' >
                                @endif        
                                {{$q->groups->group}}{{$q->number}}
                            </button>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @stop
        
        @section('foot')
        <script type="text/javascript">
            $(function() {
                document.getElementById("queuePage").className += " active";
            });
            function select(group,id){
                document.getElementById('group'+group).value=id;
                return false;
            }
            
            document.getElementById("edit1").onsubmit=function(){
                return isvalid();
            }
            
            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#alert").slideUp(500);
            });
            
            function close(){
                $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#alert").slideUp(500);
                });
            }
            
            setInterval(ajaxQueue, 8000);
            
            function ajaxQueue() {
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: '{{route("API.ajaxAdmin")}}',
                    success: function(data) {
                        html ='';
                        for (var i =0; i< data.length;i++){
                            html+='<div class="col-sm-3">';
                                html+='<div class="col-sm-12 myCol1  bg-primary">';
                                    html+='<form action="" onsubmit="return isvalid()" method="POST" id=edit'+data[i][0].idg+'>';
                                        html+='<input type="hidden" id=group'+data[i][0].idg+' readonly required>';
                                        html+='<input type="hidden" id=type'+data[i][0].idg+' name="type" readonly required>';
                                        
                                        if(i==data.length-1){
                                            html+='<center><h1>'+data[i][0].group+' ('+data[i][0].min+'+) '+data[i].length+'</h1></center>';
                                        }else{
                                            html+='<center><h1>'+data[i][0].group+' ('+data[i][0].min+'-'+data[i][0].max+') '+data[i].length+'</h1></center>';
                                        }
                                        html+='<input type="hidden" name="_method" value="PUT">';
                                        html+='<input type="hidden" name="_token" value="{{ csrf_token() }}">';
                                        html+='<div class="col-sm-12">';
                                            html+='<button class="btn btn-primary" type="submit" onclick="return edittype(0,'+data[i][0].idg+')"><span class="glyphicon glyphicon-chevron-up"></span></button>';
                                            html+='<button class="btn btn-primary" type="submit" onclick="return edittype(1,'+data[i][0].idg+')"><span class="glyphicon glyphicon-arrow-up"></span></button>';
                                            html+='<button class="btn btn-primary" type="submit" onclick="return edittype(2,'+data[i][0].idg+')"><span class="glyphicon glyphicon-chevron-down"></span></button>';
                                            html+='<button class="btn btn-primary" type="submit" onclick="return edittype(3,'+data[i][0].idg+')"><span class="glyphicon glyphicon-arrow-down"></span></button>';
                                            html+='</div>';
                                            html+='<div class="col-sm-12">';
                                                html+='<button class="btn btn-success col-sm-4 col-sm-offset-1" type="submit" onclick="return edittype(4,'+data[i][0].idg+')"><span class="glyphicon glyphicon-earphone"></span></button>';
                                                html+='<button class="btn btn-danger col-sm-4 col-sm-offset-2" type="submit" onclick="return edittype(5,'+data[i][0].idg+')"><span class="glyphicon glyphicon-ok"></span></button>';
                                                html+='</div></form>';
                                                html+='<div class="col-sm-12 myCol2" style="overflow-y:scroll;height: 50vh">';
                                                    for (var j=0; j<data[i].length; j++){
                                                        html+='<div class="col-sm-12" style="height:10px;"></div>';
                                                        if (data[i][j].status==0){
                                                            html+='<button class="col-sm-12 btn btn-danger myButton" onclick="return select('+data[i][j].idg+','+data[i][j].idq+')" >';
                                                            }else if(data[i][j].status==1){
                                                                html+='<button class="col-sm-12 btn btn-warning myButton" onclick="return select('+data[i][j].idg+','+data[i][j].idq+')" >';
                                                                }else if(data[i][j].status==2){
                                                                    html+='<button class="col-sm-12 btn btn-success myButton" onclick="return select('+data[i][j].idg+','+data[i][j].idq+')" >';
                                                                    }
                                                                    html+=data[i][j].group+data[i][j].number+'</button>';
                                                                }
                                                                html+='</div></div></div>';
                                                            }
                                                            document.getElementById('table').innerHTML=html;
                                                            
                                                        }
                                                    });
                                                }
                                                
                                                function edittype(type,group){
                                                    id=document.getElementById('group'+group).value;
                                                    if(id==""){
                                                        isi=document.getElementById('alert').innerHTML;
                                                        if(isi==""){
                                                            html="<div class='alert alert-danger'>";
                                                                html+="<strong>Notification</strong> You need to choose the queue first!</div>";
                                                                document.getElementById("alert").innerHTML= html;
                                                            }else{
                                                                $("#alert").slideDown();
                                                                close();
                                                            }
                                                            return false;
                                                        }
                                                        document.getElementById('type'+group).value=type;
                                                        frm = document.getElementById('edit'+group);
                                                        frm.action='/queue/'+id;
                                                    }
                                                </script>
                                                
                                                <!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
                                                    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                                                    <script>
                                                        $(function () {
                                                            $('#example1').DataTable()
                                                            $('#example2').DataTable({
                                                                'paging'      : true,
                                                                'lengthChange': false,
                                                                'searching'   : false,
                                                                'ordering'    : true,
                                                                'info'        : true,
                                                                'autoWidth'   : false
                                                            })
                                                        })
                                                    </script>-->
                                                    @stop