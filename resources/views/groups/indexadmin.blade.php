@extends('includes.main')
@section('head')

<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('path')
<h1>
  Group
  <small>Manage Group</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="#">Group</a></li>
  <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Group Form</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form action='' method='POST'>
    <div class="box-body">
        <div class="form group col-sm-12">
            <label>Group</label>
            <input class="form-control" type='text' name='group' required>
        </div>
        <div class="form group col-sm-6">
            <label>Min</label>
            <input class="form-control" type='text' name='min' required>
        </div>
        <div class="form group col-sm-6">
            <label>Max</label>
            <input class="form-control" type='text' name='max' required>
        </div>
    </div>
    <div class="box-footer">
        <div class="form group col-sm-12">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn pull-right myButton1" > Add</button>
        </div>
    </div>
</form>
</div>

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Group Table</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Group</th>
            <th>Min</th>
            <th>Max</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($group as $g)
        <tr>
            <td>{{$g->id}}</td>
            <td>{{$g->group}}</td>
            <td>{{$g->min}}</td>
            <td>{{$g->max}}</td>
            <td><button class="btn btn-warning" data-hover="tooltip" data-placement="top" data-target="#editModal" data-toggle="modal" id="modal-edit" data-id={{$g->id}} data-group={{$g->group}} data-min={{$g->min}} data-max={{$g->max}} title="Edit" >Edit</button>
            </td>
            <td><button class="btn btn-danger" data-hover="tooltip" data-placement="top" data-target="#deleteModal" data-toggle="modal" id="modal-edit" data-id={{$g->id}} data-group={{$g->group}} data-min={{$g->min}} data-max={{$g->max}} title="Delete" >Delete</button></td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
</div>
<!--EDIT MODAL-->
<div class="modal modal-warning fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Group</h4>
      </div>

      <form action='' method='POST' id="search" enctype="multipart/form-data">
          <div class="modal-body col-sm-12">
            <div class="form group col-sm-12">
                <label>Group</label>
                <input class="form-control group" id="group" type='text' name='group' required>
            </div>
            <div class="form group col-sm-6">
                <label>Min</label>
                <input class="form-control" id="min" type='text' name='min' required>
            </div>
            <div class="form group col-sm-6">
                <label>Max</label>
                <input class="form-control" id="max" type='text' name='max' required>
            </div>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="modal-footer col-sm-12">
            <button type="submit" class="btn btn-warning pull-right"> Save changes</button>
        </div>
    </form>
</div>
</div>
</div>
<!--END EDIT MODAL-->
<!-- DELETE MODAL -->
<div class="modal modal-danger fade" id="deleteModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Group</h4>
        </div>

        <form action='' method='POST' id="search2" enctype="multipart/form-data">
            <div class="modal-body col-sm-12">
               <div class="form group col-sm-12">
                <label>Group</label>
                <input class="form-control group" id="group" type='text' name='group' readonly>
            </div>
            <div class="form group col-sm-6">
                <label>Min</label>
                <input class="form-control" id="min" type='text' name='min' readonly>
            </div>
            <div class="form group col-sm-6">
                <label>Max</label>
                <input class="form-control" id="max" type='text' name='max' readonly>
            </div>
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="modal-footer col-sm-12">
          <button type="submit" class="btn btn-danger pull-right"> Delete</button>
      </div>
  </form>
</div>
</div>
</div>
<!-- END DELETE MODAL -->
</div>
@stop
@section('foot')
<script type="text/javascript">
    $(function() {
        document.getElementById("groupPage").className += " active";
    });
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var group = button.data('group')
        var min = button.data('min')
        var max = button.data('max')
        var modal = $(this)
        frm = document.getElementById('search');
        frm.action="/group/"+id;
        modal.find('#group').val(group)
        modal.find('#min').val(min)
        modal.find('#max').val(max)
        
    })
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var group = button.data('group')
        var min = button.data('min')
        var max = button.data('max')
        var modal = $(this)
        frm = document.getElementById('search2');
        frm.action="/group/"+id;
        modal.find('#group').val(group)
        modal.find('#min').val(min)
        modal.find('#max').val(max)    
    })
</script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
    })
  })
</script>-->
@stop