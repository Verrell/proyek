@extends('includes.main')
@section('head')

<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('path')
<h1>
  User
  <small>Manage User</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  <li><a href="#">User</a></li>
  <li class="active">Data tables</li>
</ol>
@stop
@section('content')
<div class="col-sm-8 col-sm-offset-2">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">User Form</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form action='' method='POST'>
    <div class="box-body">
        <div class="form group col-sm-12">
            <label>Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="form group col-sm-12">
            <label>E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form group col-sm-12">
            <label>Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="box-footer">
        <div class="form group col-sm-12">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn pull-right myButton1" > Add</button>
        </div>
    </div>
</form>
</div>

<div class="box">
    <div class="box-header">
      <h3 class="box-title">User Table</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user as $g)
        <tr>
            <td>{{$g->id}}</td>
            <td>{{$g->name}}</td>
            <td>{{$g->email}}</td>
            <td>{{$g->role}}</td>
            <td><button class="btn btn-warning" data-hover="tooltip" data-placement="top" data-target="#editModal" data-toggle="modal" id="modal-edit" data-role={{$g->role}} data-id={{$g->id}} data-nama={{$g->name}} data-email={{$g->email}} data-password={{$g->password}} title="Edit" >Edit</button>
            </td>
            <td><button class="btn btn-danger" data-hover="tooltip" data-placement="top" data-target="#deleteModal" data-toggle="modal" id="modal-edit" data-role={{$g->role}} data-id={{$g->id}} data-nama={{$g->name}} data-email={{$g->email}} data-password={{$g->password}} title="Delete" >Delete</button></td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
</div>
<!--EDIT MODAL-->
<div class="modal modal-warning fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit User</h4>
      </div>

      <form action='' method='POST' id="search" enctype="multipart/form-data">
          <div class="modal-body col-sm-12">
            <div class="form group col-sm-12">
                <label>Nama</label>
                <input class="form-control group" id="nama1" type='text' name='nama' required>
            </div>
            <div class="form group col-sm-12">
                <label>Email</label>
                <input class="form-control" id="email1" type='text' name='email' required>
            </div>
            <div class="form group col-sm-12">
                <label>Password</label>
                <input class="form-control" id="password1" type='text' name='password' required>
            </div>
            <div class="form group col-sm-12">
                <label>Role</label>
                <input class="form-control" id="role1" type='text' name='role' required>
            </div>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="modal-footer col-sm-12">
            <button type="submit" class="btn btn-warning pull-right"> Save changes</button>
        </div>
    </form>
</div>
</div>
</div>
<!--END EDIT MODAL-->
<!-- DELETE MODAL -->
<div class="modal modal-danger fade" id="deleteModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Group</h4>
        </div>

        <form action='' method='POST' id="search2" enctype="multipart/form-data">
            <div class="modal-body col-sm-12">
             <div class="form group col-sm-12">
                <label>Nama</label>
                <input class="form-control group" id="nama2" type='text' name='nama' readonly>
            </div>
            <div class="form group col-sm-12">
                <label>Email</label>
                <input class="form-control" id="email2" type='text' name='email' readonly>
            </div>
            <div class="form group col-sm-12">
                <label>Password</label>
                <input class="form-control" id="password2" type='text' name='password' readonly>
            </div>
            <div class="form group col-sm-12">
                <label>Role</label>
                <input class="form-control" id="role2" type='text' name='role' readonly>
            </div>
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="modal-footer col-sm-12">
          <button type="submit" class="btn btn-danger pull-right"> Delete</button>
      </div>
  </form>
</div>
</div>
</div>
<!-- END DELETE MODAL -->
</div>
@stop
@section('foot')
<script type="text/javascript">
    $(function() {
        document.getElementById("userPage").className += "active";
    });
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var nama = button.data('nama')
        var email = button.data('email')
        var password = button.data('password')
        var role = button.data('role')
        var modal = $(this)
        frm = document.getElementById('search');
        frm.action="/admin/"+id;
        modal.find('#nama1').val(nama)
        modal.find('#email1').val(email)
        modal.find('#password1').val(password)
        modal.find('#role1').val(role)
    })
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var nama = button.data('nama')
        var email = button.data('email')
        var password = button.data('password')
        var role = button.data('role')
        var modal = $(this)
        frm = document.getElementById('search2');
        frm.action="/admin/"+id;
        modal.find('#nama2').val(nama)
        modal.find('#email2').val(email)
        modal.find('#password2').val(password)
        modal.find('#role2').val(role)
    })
</script>
<!--<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
    })
  })
</script>-->
@stop