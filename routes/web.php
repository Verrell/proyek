<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::put('/public/{id}', 'QueueController@updatephone');

Route::get('/coba', 'QueueController@coba');

Route::get('/public/{id?}', 'QueueController@indexCustomer');

Route::get('/event',function(){
    event(new App\Events\Event("hallo"));
});

Route::Group(['middleware'=>'auth'],function(){
    Route::resource('/queue', 'QueueController');
    Route::post('/reset', 'QueueController@reset');
    Route::resource('/group', 'GroupController');
    Route::resource('/videos', 'VideoController');
    Route::resource('/admin', 'UserController');
    Route::resource('/setting', 'SettingController');
    Route::get('/android', 'SettingController@android');
    Route::resource('/report', 'ReportController');
    Route::get('/configure', 'SettingController@configure');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
