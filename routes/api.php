<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/video','VideoController@getVideo')->name('API.video');
Route::get('/foto','VideoController@getFoto')->name('API.foto');
Route::get('/queue2','QueueController@getAjax')->name('API.ajax');
Route::get('/queue3','QueueController@getAjaxAdmin')->name('API.ajaxAdmin');
Route::put('/spark','QueueController@updatespark')->name('API.spark');
Route::get('/report','ReportController@getReport')->name('API.report');

Route::get('/setting','SettingController@getSetting')->name('API.setting');
Route::post('/queue','QueueController@getQueue')->name('API.queue');
Route::post('/token','QueueController@getToken')->name('API.token');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});